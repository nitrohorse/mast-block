const fetch = require('node-fetch');
const domainsToBlock = require('./domains-to-block');

const myInstanceDomain = ''; // e.g. nitro.horse
const log = console.log;

const rememberUserToken = '';
const sessionId = '';
const mastodonSession = '';
const authenticityToken = '';
const severity = ''; // suspend, silence

const blockDomain = async (domain) => {
	log(`Blocking ${domain}...`);
	await fetch(`https://${myInstanceDomain}/admin/domain_blocks`, {
		'headers': {
			'content-type': 'application/x-www-form-urlencoded',
			'cookie': `remember_user_token=${rememberUserToken}; _session_id=${sessionId}; _mastodon_session=${mastodonSession}`
		},
		'body': `utf8=%E2%9C%93&authenticity_token=${authenticityToken}&domain_block%5Bdomain%5D=${domain}&domain_block%5Bseverity%5D=${severity}&domain_block%5Breject_media%5D=0&domain_block%5Breject_reports%5D=0&domain_block%5Bprivate_comment%5D=&domain_block%5Bpublic_comment%5D=&button=`,
		'method': 'POST',
		'mode': 'cors'
	});
	log(`Blocked ${domain}`);
}

domainsToBlock.forEach(async (domain) => {
	await blockDomain(domain);
});
