# Mast Block

Mass block domains from federating with your Mastodon instance.

## Setup

1. Clone the repo
1. Install tools
	* [Node.js](https://nodejs.org)
	* [nvm](https://github.com/nvm-sh/nvm)
1. Use specified Node version
	* `nvm use`
1. Install dependencies
	* `npm i`


## Run

1. Add required values to `index.js`. These can be found by inspecting a request via browser tools (specifically selecting "Copy as Node.js fetch") while logged into your Mastodon instance.
    * `rememberUserToken`
    * `sessionId`
    * `mastodonSession`
    * `authenticityToken`
    * `severity` (suspend or silence)
1. Add domains you want to block to `domains-to-block.js`.
1. Run
    * `npm start`
1. Validate by navigating to `https://<your instance>/admin/instances`.